FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY "wasm-bindgen-bin" "/usr/local/bin/wasm-bindgen"
COPY "wasm-bindgen-test-runner-bin" "/usr/local/bin/wasm-bindgen-test-runner"
COPY "wasm2es6js-bin" "/usr/local/bin/wasm2es6js"
