#!/bin/sh

sed '/rustwasm\/wasm-bindgen/!d' version_manifest.txt | tail -n 1 | cut -d' ' -f5 | cut -d'=' -f2
